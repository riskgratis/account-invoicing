This module add a warning popup on invoice to ensure warning is populated
no only when partner is changed.

This popup could be configured for customer invoices, vendor ones, or both.
