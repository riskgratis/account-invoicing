By default Odoo adds customer as follower to all invoices.
This module allows to disable this behaviour using General Settings.
